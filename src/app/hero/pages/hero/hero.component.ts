import { Component, OnInit } from '@angular/core';
import {delay, switchMap} from 'rxjs/operators';
import {Hero} from '../../interface/hero.interface';
import {ActivatedRoute, Router} from '@angular/router';
import {HeroService} from '../../service/hero.service';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styles: [
  ]
})
export class HeroComponent implements OnInit {
  detailsHero!: Hero;

  constructor(private activatedRoute: ActivatedRoute,
              private heroService: HeroService,
              private router: Router) { }

  ngOnInit(): void {
    const idHero = this.activatedRoute.params;
    if (idHero) {
      idHero.pipe(switchMap(({id}) => this.heroService.getHeroId(id)),
        delay(1500)
      )
        .subscribe((resp: Hero) => this.detailsHero =  resp);
    }
  }

  onBack(): void{
    this.router.navigate(['/hero']);
  }

}
