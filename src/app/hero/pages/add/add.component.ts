import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Hero, Publisher} from '../../interface/hero.interface';
import {HeroService} from '../../service/hero.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {switchMap} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmComponent} from '../../component/confirm/confirm.component';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styles: []
})
export class AddComponent implements OnInit {

  publicSher = [
    {
      id: 'DC Comics',
      desc: 'DC - Comics'
    },
    {
      id: 'Marvel Comics',
      desc: 'Marvel - Comics'
    }
  ];

  hero: Hero = {
    superhero: '',
    alter_ego: '',
    characters: '',
    first_appearance: '',
    publisher: Publisher.DCComics,
    alt_img: ''
  };

  constructor(private heroService: HeroService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    if (!this.router.url.includes('edit')) {
      return;
    }

    this.activatedRoute.params.pipe(switchMap(({id}) => this.heroService.getHeroId(id)))
      .subscribe((hero: Hero) => this.hero = hero);
  }

  saveForm(): void {
    if (this.hero.superhero.trim().length <= 0) {
      return;
    }

    if (this.hero.id) {
      this.heroService.updateHero(this.hero)
        .subscribe((resp: Hero) => {
          this.showMsgBar('Success fully register update');
        });
    } else {
      this.heroService.addHero(this.hero)
        .subscribe((resp: Hero) => {
          this.showMsgBar('Success fully register');
          this.router.navigate(['/hero/edit', resp.id]);
        });
    }


  }

  showMsgBar(msg: string): void {
    this.snackBar.open(msg, 'Ok!', {
      duration: 2500
    });
  }


  eraseHero(): void{
    const dialog = this.dialog.open(ConfirmComponent,{
      width: '250px',
      data: this.hero
    });

    dialog.afterClosed().subscribe((result: boolean) => {
      console.log(result);
      if (result){
        this.heroService.eraseHero(this.hero.id)
          .subscribe((resp: any) => {
            return this.router.navigate(['/hero']);
          });
      }
    });
  }

}
