import {Component, OnInit} from '@angular/core';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {Hero} from '../../interface/hero.interface';
import {HeroService} from '../../service/hero.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [`
    .full-width {
      width: 100%;
      margin-top: 20px;
    }
  `]
})
export class SearchComponent implements OnInit {

  term: string = '';
  listHero: Hero[] = [];
  heroSelect: Hero | undefined;

  constructor(private heroService: HeroService) {
  }

  ngOnInit(): void {
  }

  search(): void {
    this.heroService.getSuggestion(this.term)
      .subscribe((hero: Hero[]) => this.listHero = hero);
  }

  optionSelect(event: MatAutocompleteSelectedEvent): void {

    if (!event.option.value) {
      this.heroSelect = undefined;
      return;
    }

    const {value} = event.option;
    this.term = value.superhero;

    this.heroService.getHeroId(value.id)
      .subscribe((hero: Hero) => this.heroSelect = hero);

  }

}
