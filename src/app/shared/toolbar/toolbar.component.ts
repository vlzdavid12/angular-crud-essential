import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styles: [`.space{
    flex: 1 1 auto
  }`]
})
export class ToolbarComponent implements OnInit {

  @Input('sideNav') sidenav!: any;

  constructor() { }

  ngOnInit(): void {
  }

}
