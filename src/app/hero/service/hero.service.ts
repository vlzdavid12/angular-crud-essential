import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Hero} from '../interface/hero.interface';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor(private http: HttpClient) { }

  getHero(): Observable<Hero[]> {
    return this.http.get<Hero[]>(`${environment.urlAPI}/heroes`);
  }

  getHeroId(id: string = ''): Observable<Hero>{
      return this.http.get<Hero>(`${environment.urlAPI}/heroes/${id}`);
  }

  getSuggestion(term: string): Observable<Hero[]>{
      return this.http.get<Hero[]>(`${environment.urlAPI}/heroes?q=${term}&_limit=6`);
  }

  addHero(hero: Hero): Observable<Hero>{
    return this.http.post<Hero>(`${environment.urlAPI}/heroes`, hero);
  }

  updateHero(hero: Hero): Observable<Hero>{
    return this.http.put<Hero>(`${environment.urlAPI}/heroes/${hero.id}`, hero);
  }

  eraseHero(id: string | undefined): Observable<any> {
    return this.http.delete(`${environment.urlAPI}/heroes/${id}`);
  }

}
