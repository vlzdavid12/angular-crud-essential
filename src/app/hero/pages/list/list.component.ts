import { Component, OnInit } from '@angular/core';
import {HeroService} from '../../service/hero.service';
import {Hero} from '../../interface/hero.interface';
import {Observable} from 'rxjs';
import {map, tap, distinctUntilKeyChanged} from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styles: [
  ]
})
export class ListComponent implements OnInit {

  listHero: Hero[] = [];

  constructor(private heroService: HeroService) { }

  ngOnInit(): void {
    const data: Observable<Hero[]> = this.heroService.getHero();
    data.subscribe((hero: Hero[]) => this.listHero = hero );
  }

}
