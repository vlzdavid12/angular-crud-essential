import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AddComponent } from './pages/add/add.component';
import { SearchComponent } from './pages/search/search.component';
import { ViewComponent } from './pages/view/view.component';
import { HomeComponent } from './pages/home/home.component';
import { ListComponent } from './pages/list/list.component';
import {AppRoutingModule} from './app-routing.module';
import {MaterialModule} from '../material/material.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import { CardComponent } from './component/card/card.component';
import {SharedModule} from '../shared/shared.module';

import { ImagePipe } from './pipes/image.pipe';
import { HeroComponent } from './pages/hero/hero.component';
import { ConfirmComponent } from './component/confirm/confirm.component';
import { StopClickDirective } from './directive/stop-click.directive';




@NgModule({
  declarations: [
    AddComponent,
    SearchComponent,
    ViewComponent,
    HomeComponent,
    ListComponent,
    CardComponent,
    ImagePipe,
    HeroComponent,
    ConfirmComponent,
    StopClickDirective,
  ],
    imports: [
        AppRoutingModule,
        CommonModule,
        MaterialModule,
        FlexLayoutModule,
        MatToolbarModule,
        SharedModule,
        FormsModule,
    ]
})
export class HeroModule { }
