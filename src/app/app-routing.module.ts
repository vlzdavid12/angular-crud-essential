import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PageErrorComponent} from './shared/pages/page-error/page-error.component';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'hero',
    loadChildren: () =>  import('./hero/hero.module').then(m => m.HeroModule)
  },
  {
    path: '404',
    component: PageErrorComponent
  },
  {
    path: '**',
    redirectTo: '404'
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
}
